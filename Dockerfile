# mes-lesson-planning-frontend

FROM docker:19.03.8-dind

RUN echo http://ftp.halifax.rwth-aachen.de/alpine/v3.13/main >> /etc/apk/repositories
RUN apk add docker-compose nodejs npm bash
RUN npm install -g yarn
RUN npm install -g npm-cli-login
